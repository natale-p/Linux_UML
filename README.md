# TCP testing: brotherhood between Linux and NS-3

## 0x00: About

Still to write.

List of used words:
- *host*: your host system, the operating system you have booted in;
- *sender*: The operating system which sends the data and should limit
the congestion (therefore, which uses a congestion control);
- *receiver*: The operating system which receives the data and sends the ACK;

## 0x01: Obtained results

None so far.

## 0x02: Bugs and limitations

None so far. Ahaha it's a joke...

## 0x03 Download of the required software

Please download the necessary software through git. Probably there will
never be a packaged release.

```
$ cd work_dir
$ git clone https://github.com/kronat/Linux_UML.git
$ git clone https://github.com/kronat/linux.git
$ git clone https://github.com/kronat/ns-3-dev.git
```

## 0x04 Creating the filesystem images

With these steps, we'll create an image file which contains a base
ArchLinux installation. I currently do not support
{Debian,Ubuntu,Anything}-based images. Please contribute the instructions
by following the fork/pull request procedure.

```
$ cd work_dir/Linux_UML
$ mkdir fs
$ cd fs
$ dd if=/dev/zero of=fs/fs.ext4 seek=3000 count=1 bs=1M
  1+0 records in
  1+0 records out
  1048576 bytes (1.0 MB, 1.0 MiB) copied, 0.00154869 s, 677 MB/s

$ ls -lh fs
  total 1.0M
  -rw-r--r-- 1 nat users 3001M Feb  8 12:09 fs.ext4

$ mkfs.ext4 fs/fs.ext4
  mke2fs 1.43.3 (04-Sep-2016)
  Discarding device blocks: done
  Creating filesystem with 256256 4k blocks and 64128 inodes
  Filesystem UUID: fddaf25c-38a1-4869-a7ef-44ed9bc5f061
  Superblock backups stored on blocks:
  	32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (4096 blocks): done
    Writing superblocks and filesystem accounting information: done
```

Right now we have an ext4 filesystem. Now it's time to install a base
system on top of it.

```
$ mkdir fs/loop
$ su -c "mount -o rw,loop fs/fs.ext4 fs/loop"
$ su -c "pacstrap fs/loop"
$ su -c "systemd-nspawn -b -D fs/loop"
```

The last command is used to chroot-on-steroids inside the new system.
The objective is to do, in the spawned system, the following:

```
# systemctl set-default multi-user.target
```

To disable the graphical target (we don't need it). Now install iperf:

```
# pacman -S iperf
# poweroff
```

An already made image is available here:
https://drive.google.com/open?id=0B25torSWoyRYOTREcUJaTUE1Qlk

## 0x05: Kernel config and compile

We need to compile Linux to run as a userspace process. The architecture
to do so is called User Mode Linux. In the host system:

```
$ cd work_dir/linux
$ git checkout tcp_wave
$ cp ../Linux_UML/config_uml .config
$ ARCH=um make linux -j14
```

Using a newer kernel version than the one used to generate the config
will lead to a number of questions. Use the default value, often is
the best choice. Now, add the cap_net_raw and cap_net_admin capabilities
to the generated binary.

```
# setcap cap_net_raw,cap_net_admin=eip ./linux
```

(you can use sudo or su -c to do the job instead of being logged as root).

## 0x06: Setup the network in the host

We should create the virtual devices that will connect the Linux instance
and the ns-3 instance. Please check to have installed net-tools (UML relies
on the old tool ifconfig) and bridge-utils. On the host, run the following
as root:

```
ip tuntap add dev tap-left mode tap
ip tuntap add dev tap-right mode tap
ip addr add 10.0.0.1 dev tap-left
ip addr add 10.0.1.1 dev tap-right
ip link set tap-left up
ip link set tap-right up
brctl addbr br0
brctl addif br0 tap-left tap-right
ip addr add 10.1.1.1 dev br0
ip link set br0 up

echo 1 > /proc/sys/net/ipv4/ip_forward
```

Or use the provided script "start_networking.sh".

## 0x07: Running UML

Be sure to have xterm package installed.

```
$ cd work_dir
$ ./linux/linux mem=512M ubd0=Linux_UML/fs/fs.ext4 umid=local0
```

After the booting process, an xterm window will be opened. This is
your door inside the UML, the Linux kernel as an userspace process. If the
terminal does not open, attach to the virtual pts drivers created. For instance:

```
$ ./linux-kernel mem=512M ubd0=fs/fs.ext4 eth0=tuntap,tap-left,,10.0.0.1 umid=local0
Core dump limits :
	soft - NONE
	hard - NONE
Checking that ptrace can change system c[ruby-2.4.0p0]all numbers...OK
Checking syscall emulation patch for ptrace...OK
Checking advanced syscall emulation patch for ptrace...OK
Checking environment variables for a tempdir...none found
Checking if /dev/shm is on tmpfs...OK
Checking PROT_EXEC mmap in /dev/shm...OK
Adding 11923456 bytes to physical memory to account for exec-shield gap
Linux version 4.10.0-rc8-uml-00205-g7089db84e356-dirty (nat@judith) (gcc version 6.3.1 20170109 (GCC) ) #19 Mon Feb 13 16:32:53 CET 2017
Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 131889
Kernel command line: mem=512M ubd0=fs/fs.ext4 eth0=tuntap,tap-left,,10.0.0.1 root=98:0
PID hash table entries: 4096 (order: 3, 32768 bytes)
Dentry cache hash table entries: 131072 (order: 8, 1048576 bytes)
Inode-cache hash table entries: 65536 (order: 7, 524288 bytes)
Memory: 509848K/535932K available (2647K kernel code, 697K rwdata, 772K rodata, 111K init, 171K bss, 26084K reserved, 0K cma-reserved)
NR_IRQS:15
clocksource: timer: mask: 0xffffffffffffffff max_cycles: 0x1cd42e205, max_idle_ns: 881590404426 ns
Calibrating delay loop... 6963.20 BogoMIPS (lpj=34816000)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 2048 (order: 2, 16384 bytes)
Mountpoint-cache hash table entries: 2048 (order: 2, 16384 bytes)
Checking that host ptys support output SIGIO...Yes
Checking that host ptys support SIGIO on close...No, enabling workaround
devtmpfs: initialized
Using 2.6 host AIO
clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
NET: Registered protocol family 16
clocksource: Switched to clocksource timer
VFS: Disk quotas dquot_6.6.0
VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
NET: Registered protocol family 2
TCP established hash table entries: 8192 (order: 4, 65536 bytes)
TCP bind hash table entries: 8192 (order: 4, 65536 bytes)
TCP: Hash tables configured (established 8192 bind 8192)
UDP hash table entries: 512 (order: 2, 16384 bytes)
UDP-Lite hash table entries: 512 (order: 2, 16384 bytes)
NET: Registered protocol family 1
console [stderr0] disabled
mconsole (version 2) initialized on /home/nat/.uml/local0/mconsole
Checking host MADV_REMOVE support...OK
futex hash table entries: 256 (order: 0, 6144 bytes)
workingset: timestamp_bits=46 max_order=17 bucket_order=0
Block layer SCSI generic (bsg) driver version 0.4 loaded (major 254)
io scheduler noop registered
io scheduler deadline registered (default)
NET: Registered protocol family 17
Initialized stdio console driver
Console initialized on /dev/tty0
console [tty0] enabled
Initializing software serial port version 1
console [mc-1] enabled
Choosing a random ethernet address for device eth0
Netdevice 0 (c6:40:9b:5b:65:2a) :
TUN/TAP backend - IP = 10.0.0.1
EXT4-fs (ubda): couldn't mount as ext3 due to feature incompatibilities
EXT4-fs (ubda): couldn't mount as ext2 due to feature incompatibilities
EXT4-fs (ubda): INFO: recovery required on readonly filesystem
EXT4-fs (ubda): write access will be enabled during recovery
EXT4-fs (ubda): recovery complete
EXT4-fs (ubda): mounted filesystem with ordered data mode. Opts: (null)
VFS: Mounted root (ext4 filesystem) readonly on device 98:0.
devtmpfs: mounted
This architecture does not have kernel memory protection.
random: fast init done
systemd[1]: systemd 232 running in system mode. (+PAM -AUDIT -SELINUX -IMA -APPARMOR +SMACK -SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN)
systemd[1]: Detected virtualization uml.
systemd[1]: Detected architecture x86-64.

Welcome to Arch Linux!

systemd[1]: No hostname configured.
systemd[1]: Set hostname to <localhost>.
systemd[1]: Failed to enable kbrequest handling: Inappropriate ioctl for device
systemd[1]: Reached target Swap.
[  OK  ] Reached target Swap.
systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Started Forward Password Requests to Wall Directory Watch.
systemd[1]: Listening on Process Core Dump Socket.
[  OK  ] Listening on Process Core Dump Socket.
systemd[1]: Created slice System Slice.
[  OK  ] Created slice System Slice.
         Mounting POSIX Message Queue File System...
[  OK  ] Listening on LVM2 metadata daemon socket.
[  OK  ] Listening on /dev/initctl Compatibility Named Pipe.
[  OK  ] Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Reached target Encrypted Volumes.
[  OK  ] Listening on Device-mapper event daemon FIFOs.
[  OK  ] Listening on Journal Socket.
         Starting Remount Root and Kernel File Systems...
[  OK  ] Reached target Remote File Systems.
[  OK  ] Reached target Paths.
[  OK  ] Created slice User and Session Slice.
[  OK  ] Reached target Slices.
[  OK  ] Listening on udev Kernel Socket.
[  OK  ] Created slice system-getty.slice.
         Starting Apply Kernel Variables...
[  OK  ] Listening on Journal Socket (/dev/log).
         Starting Journal Service...
         Mounting Temporary Directory...
[  OK  ] Set up automount Arbitrary Executable File Formats File System Automount Point.
[  OK  ] Listening on udev Control Socket.
[  OK  ] Mounted Temporary Directory.
[  OK  ] Mounted POSIX Message Queue File System.
[  OK  ] Started Remount Root and Kernel File Systems.
[  OK  ] Started Apply Kernel Variables.
         Starting udev Coldplug all Devices...
         Starting Load/Save Random Seed...
         Starting Create Static Device Nodes in /dev...
[  OK  ] Started Journal Service.
         Starting Flush Journal to Persistent Storage...
[  OK  ] Started Load/Save Random Seed.
systemd-journald[43]: Received request to flush runtime journal from PID 1
[  OK  ] Started Flush Journal to Persistent Storage.
[  OK  ] Started Create Static Device Nodes in /dev.
[  OK  ] Reached target Local File Systems (Pre).
[  OK  ] Reached target Local File Systems.
         Starting Create Volatile Files and Directories...
         Starting udev Kernel Device Manager...
[  OK  ] Started udev Kernel Device Manager.
[  OK  ] Started udev Coldplug all Devices.
[FAILED] Failed to start Create Volatile Files and Directories.
See 'systemctl status systemd-tmpfiles-setup.service' for details.
         Starting Update UTMP about System Boot/Shutdown...
[FAILED] Failed to start Update UTMP about System Boot/Shutdown.
See 'systemctl status systemd-update-utmp.service' for details.
[  OK  ] Reached target System Initialization.
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Reached target Sockets.
[  OK  ] Reached target Basic System.
[  OK  ] Started D-Bus System Message Bus.
         Starting Login Service...
         Starting Permit User Sessions...
[  OK  ] Started Daily verification of password and group files.
[  OK  ] Started Daily man-db cache update.
[  OK  ] Started Daily rotation of log files.
[  OK  ] Reached target Timers.
[  OK  ] Started Permit User Sessions.
[  OK  ] Started Getty on tty1.
[  OK  ] Reached target Login Prompts.
[  OK  ] Started Login Service.
Virtual console 6 assigned device '/dev/pts/4'
[  OK  ] Reached target Multi-User System.
Virtual console 1 assigned device '/dev/pts/5'
```

In the host, it is possible to do
```
$ screen /dev/pts/4
```

To attach to a shell in the guest system (use 4 or 5, as you wish, but please
note that these numbers can be different in your run). When you will be inside
screen, just type "root" and you will be logged. If nothing happens, press
Ctrl+Shift+d to detach and then attach to the other console (in the previous case, 5).

## 0x08: Configuring and compiling ns-3

In order to simplify library management, we are going to compile ns-3 in static
mode. Please note that this will require a lot of RAM in the linking phase.

```
$ cd work_dir/ns-3-dev-git
$ git checkout linux-tcp-testing
$ ./waf configure --enable-examples --disable-tests --disable-gtk --enable-sudo --enable-static -d debug --enable-module=internet,applications,tap-bridge,csma
$ ./waf
```

If waf dies, append -j1 in order to reduce the amount of RAM used. If you
manage to successfully compile it, run the example:

```
$ export PATH="$PATH:$(pwd)/build/src/tap-bridge"
$ ./build/src/tap-bridge/examples/ns3-dev-tap-p2p-virtual-receiver-debug
  main(): Provided Device Name is "tap-left"
  main(): Provided Gateway Address is "255.255.255.255"
  main(): Provided IP Address is "255.255.255.255"
  main(): Provided MAC Address is "00:00:00:00:00:01"
  main(): Provided Net Mask is "255.255.255.255"
  main(): Provided Operating Mode is "2"
  main(): Provided path is ":01:00:00:30:30:30:32:30"
  main(): Creating Tap
  CreateTap(): Allocated TAP device tap-left
  CreateTap(): Returning precreated tap
  SendSocket(): Create Unix socket
  SendSocket(): Decode address :01:00:00:30:30:30:32:30
  SendSocket(): Connect
  SendSocket(): Connected
  SendSocket(): sendmsg complete
```

The example has a lot of options to change the bandwidth, the delay and the
error rate. Please consult its help. Now the example is running, let's go back
to UML.

## 0x09: Configure the network inside UML

In the shell attached to the guest kernel, do the following:

```
# ip addr add 10.0.0.2 dev eth0
# ip link set dev eth0 up
# ip route add default via 10.0.0.2
```

Or use the provided script start_net_inside.sh on the provided guest
filesystem.

## 0x10: Stopping the guest and cleaning networking

To stop the guest, do a simple poweroff inside the guest terminal.
To remove the created TAP devices and the bridge, please do:

```
# brctl delif br0 tap-left
# brctl delif br0 tap-right
# ip link set dev br0 down
# brctl delbr br0
# tunctl -d tap-left
# tunctl -d tap-right
```

Or use the provided script stop_networking.sh.

## 0x11: Run a transfer

The basic iperf command inside the guest is the following:

```
# iperf -c 10.0.2.1
```

However, iperf adds a lot of complexity in the transfer. I provide a set of
custom apps, inside the directory apps. There you can find a server and a
client. The server reads from a file and sends over a socket, while the client
does the contrary. If you use the default image, there will be a directory apps
in which these programs will be present and already compiled. Otherwise, you
should compile them in the host and then copy them then in the guest filesystem.
Assuming that these steps are done, the best program to couple to a ns-3
receiver is "sender". In the UML shell you should do the following:

```
# cd apps
# dd if=/dev/urandom of=transmit.data bs=500 count=20
# ./sender 5001 transmit.data 10.0.2.1
```

You should be able to transfer the file transmit.data to the ns-3 end, seeing it
with wireshark in the generated pcap inside the ns-3-dev-git directory.


## 0x12: Running linux under a debugger

Start the linux kernel with gdb, by prepending "gdb --args" to its command line.

```
$ gdb --args ./linux mem=512M ...
```

Then, use the following options inside gdb:

```
handle SIGSEGV pass nostop noprint
handle SIGUSR1 pass nostop noprint
```

And then execute with run. For your convenience, there are some interesting breakpoints:

```
break tcp_v4_connect
break tcp_connect
break tcp_make_synack
break tcp_ack
break tcp_transmit_skb
```

When stopped, use c to continue the run, s to step into, n to pass to the next line.

## 0x13: Some other interesting things

The congestion control: 

```
# cat /proc/sys/net/ipv4/tcp_congestion_control
```

Set cubic:

```
# echo "cubic" > /proc/sys/net/ipv4/tcp_congestion_control
```
