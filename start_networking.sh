#!/bin/bash

# Configure and start the network

if [[ ${EUID} != 0 ]]; then
  echo "This script is meant to be run as root. Refusing to start."
  exit 1
fi

ip=$(which ip)
if [[ $? != 0 ]]; then
  echo "ip not found in your system. Please install iproute2."
  echo "(or edit the script to use the old net-tools)"
  exit 1
fi

brctl=$(which brctl)
if [[ $? != 0 ]]; then
  echo "brctl not found in your system. Please install bridge-utils."
  exit 1
fi

echo -en "Creating tun/tap devices..."
${ip} tuntap add dev tap-left mode tap
${ip} tuntap add dev tap-right mode tap
echo -en "Done.\nAdding addresses to them..."
${ip} addr add 10.0.0.1 dev tap-left
${ip} addr add 10.0.1.1 dev tap-right
echo -en "Done.\nBringing them up..."
${ip} link set tap-left up
${ip} link set tap-right up
echo -en "Done.\nAdding and configuring the bridge..."
${brctl} addbr br0
${brctl} addif br0 tap-left tap-right
${ip} addr add 10.1.1.1 dev br0
${ip} link set br0 up
echo -en "Done.\nEnabling ip_forward..."
echo 1 > /proc/sys/net/ipv4/ip_forward
echo -en "Done.\nConfiguration finished.\n"
