#!/bin/bash

# Stop the network

if [[ ${EUID} != 0 ]]; then
  echo "This script is meant to be run as root. Refusing to start."
  exit 1
fi


brctl delif br0 tap-left
brctl delif br0 tap-right
ip link set dev br0 down
brctl delbr br0
tunctl -d tap-left
tunctl -d tap-right
